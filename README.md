A project leírása:

A Cluedo nevű társasjátékból származik az app alapötlete:
- A játék során a játékosok egymás után, sorban kérdéseket tesznek fel, amiből kikövetkeztethető a játék fő kérdés-hármasa: 
    1. Ki volt a gyilkos?
    2. Mi volt a gyilkos eszköz?
    3. Melyik helyiségben történt a gyilkosság?
- Mindezt a nyomozó játékosok egy-egy, a játékhoz mellékelt papírlapon (ami szerintem nem túl részletes, elég pici, és véges mennyiségű) követik nyomon.
- Az appom ezt a papírt váltaná ki egy könyebben kezelhető, nyomon követhető felülettel, ami esetleg a játékosok közötti kötelező kommunikációt is kiválthaná 
(bár ezt még nem tudom, hogyan:D), amit mi mindig messengeren oldunk meg, hogy ne is tudják szájról leolvasni a többiek...:DD